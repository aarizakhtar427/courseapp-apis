﻿using LearningCourseApp.Models;
using LearningCourseApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace LearningCourseApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        readonly ICourseService _courseService;
        readonly ITokenGenerator _tokenGenerator;
       

        public CourseController(ICourseService courseService , ITokenGenerator tokenGenerator )
        {
            _courseService = courseService;
            _tokenGenerator = tokenGenerator;
        }
        
        [Route("getAllCourses")]
        [HttpGet]
        public ActionResult GetAllCourses()
        {
            List<Course> courses = _courseService.GetAllCourses();
            return Ok(courses);

        }
        [Route("addCourse")]
        [HttpPost]
        public ActionResult AddCourse(Course course)
        {
            bool addCourseStatus = _courseService.AddCourse(course);
            return Ok(addCourseStatus);
        }

        [Route("deleteCourse{id:int}")]
        [HttpDelete]
        public ActionResult DeleteCourse(int id)
        {
            bool courseDeleteStatus = _courseService.DeleteCourse(id);
            return Ok(courseDeleteStatus);
        }

        [Route("updateCourse{id:int}")]
        [HttpPut]
        public ActionResult UpdateCourse(int id, Course course)
        {
            bool courseUpdateStatus = _courseService.UpdateCourse(id, course);
            return Ok(courseUpdateStatus);
        }

        [Route("getCourseById{id:int}")]
        [HttpGet]
        public ActionResult GetCourseById(int id)
        {
            Course course = _courseService.GetCourseById(id);
            return Ok(course);
        }

        [Route("availableUnavailableCourse{id:int}")]
        [HttpPut]
        public ActionResult AvailableUnAvailableCourse(int id, bool availableUnavailableCourse)
        {
            bool availableUnavailableCourseStatus = _courseService.AvailableUnAvailableCourse(id, availableUnavailableCourse);
            return Ok(availableUnavailableCourseStatus);
        }

        
        [Route("loginCourse")]
        [HttpPost]
        public ActionResult LoginCourse(FindCourse findCourse)
        {
            Course course = _courseService.TokenGenerate(findCourse);
            string coursePrice = Convert.ToString(course.CoursePrice);
            string courseToken = _tokenGenerator.GenerateToken(course.Id, course.CourseName,course.CourseDescription, coursePrice);
            return Ok(courseToken);
        }









    }
}
