﻿namespace LearningCourseApp.Models
{
    public class FindCourse
    {
        public string CourseName { get; set; }
        public double CoursePrice { get; set; }
    }
}
